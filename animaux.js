function Animal(n){

    this.nom = n;
    this.age = 0;
    this.naissance = new Date();  //ajoute le jour de naissance au jour du click de création New Animal

    this.calculer_age = function(){}
    this.deplacer = function(){}
    this.mourir = function(){}

    this.afficher = function(){
        var bestiaire = document.getElementById("bestiaire");
        var div = document.createElement("DIV");
        var nom = document.createTextNode(this.nom);
        div.appendChild(nom);
        bestiaire.appendChild(div);
    }

    this.afficher();
}

function nom_aleatoire(){
    var noms = [
        "Marcel", "Médor", "Flipper", "Babe", "Rintintin", 
        "Idéfix", "Balou", "Lassie", "Jolly Jumper"
    ];
    return noms[Math.floor(Math.random()*noms.length)];
}

function donner_la_vie()
{
    let nom = nom_aleatoire();
    let animal = new Animal1(nom);
}

var bt = document.getElementById("donner_la_vie");
bt.addEventListener("click", donner_la_vie, false);
